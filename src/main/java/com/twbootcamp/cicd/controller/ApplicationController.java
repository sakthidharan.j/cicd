package com.twbootcamp.cicd.controller;

import com.twbootcamp.cicd.model.Book;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/application")
public class ApplicationController {

    @GetMapping
    public Book getBook() {
        return new Book();
    }

}
