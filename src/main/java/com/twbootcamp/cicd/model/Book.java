package com.twbootcamp.cicd.model;

public class Book {
    private String name = "book one";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
