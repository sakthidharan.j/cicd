package com.twbootcamp.cicd.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ApplicationController.class)
@RunWith(SpringRunner.class)
public class ApplicationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void whenGetBook_thenShouldReturnBookOne(){
        ApplicationController systemUnderTest = new ApplicationController();
        Assert.assertNotNull(systemUnderTest.getBook());
        Assert.assertEquals("book one", systemUnderTest.getBook().getName());
    }

    @Test
    public void whenGetAppEndPoint_thenReturnBook() throws Exception {
        mockMvc.perform(get("/application")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("book one"));
    }
}

